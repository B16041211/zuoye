#include<stdio.h>
#include<stdlib.h>
int main(int argc,char **argv){
	if(argc !=3)
	{
		printf("cpy needs 2 arguments (%d given)!\n",argc-1);exit(-1);
	}
	char * src=argv[1];
	char * dest = argv[2];
	FILE * srcf = fopen(src,"rb");
	if(!src)
	{
		puts("file error!");exit(-1);
	}
	FILE * desf = fopen(dest ,"wb+");
	if(!desf)
	{
		puts("file error!");exit(-1);
	}
	char c = fgetc(srcf);
	while(c!=EOF){
		fputc(c,desf);
		c = fgetc(srcf);
	}
	fclose(srcf);
	fclose(desf);
	return;