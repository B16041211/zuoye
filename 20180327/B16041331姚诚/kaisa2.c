#include<stdio.h>
#include<string.h>
int main(){
	printf("Input:");
	char c=getchar();
	printf("Output:");
	while(c!='\n'){
		c=(c>='a'&&c<='z')?(c-'a'+2)%26 +'a':(c>='A'&&c<='Z')?(c-'A'+2)%26 +'A':(c>='0'&&c<='9')?((c-'0'+2)%10)+'0':0;
		putchar(c);
		c=getchar();
	}
	puts("");
	return 0;
}