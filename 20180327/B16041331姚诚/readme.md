
## kaisa
小写字母凯撒加密(offset=2)

```
$ ./kaisa
Input:abcdxyz 
Output:cdefzab

```

## kaisa2

大小写英文字母和数字凯撒加密(offset=2)

```
$ ./kaisa
Input:abcxyz
Output:cdezab
$ ./kaisa
Input:ABCXYZ
Output:CDEZAB
$ ./kaisa
Input:012789
Output:	
$ ./kaisa
Input:012789
Output:234901

```


## cpy
文件拷贝

```
./cpy kaisa.c kai.c

$cat kaisa.c

#include<stdio.h>
#include<string.h>
int main(){
	printf("Input:");
	char c=getchar();
	printf("Output:");
	while(c!='\n'){
		c=(c-'a'+2)%26 +'a';
		putchar(c);
		c=getchar();
	}
	puts("");

$cat kai.c
#include<stdio.h>
#include<string.h>
int main(){
	printf("Input:");
	char c=getchar();
	printf("Output:");
	while(c!='\n'){
		c=(c-'a'+2)%26 +'a';
		putchar(c);
		c=getchar();
	}
	puts("");


```