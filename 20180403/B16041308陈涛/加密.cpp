#include<stdio.h>
#include<string.h>
char strlwr(char a)
{
	char x=a;
	int i;
	if(x>='a'&&x<='z')
	{
		x=(x-3-'z')%26+'z';
		x=x-32;
	}
	else if(x>='A'&&x<='Z')
	{
		x=(x-3-'Z')%26+'Z';
		x=x+32;
	}
	return x; 
}
int main()
{
	char str[20];
	int i=0,j,temp,k;
	for(i=0;i<20;i++)
		str[i]='A';    //初始化，便于观察
	printf("请输入密令：") ;
	gets(str);
	k=strlen(str);
	for(i=0;i<k/2;i++)
	{
		temp=str[i];
		str[i]=str[k-1-i];
		str[k-i-1]=temp;	
	}
	i=0;    //务必注意，在for循环后面的i的值，若无此句，这很容易造成后面的变量*无法成功赋值 *
	printf("加密后："); 
	while(i<k)
	{
		str[i]=strlwr(str[i]);
		putchar(str[i]);
		i++;
	}
	//puts(str);
		return 0;
}
