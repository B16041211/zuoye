#include <iostream>
#include <string>
using namespace std;

class ReverseStr
{
public:
	void reverseWords(string &s)
	{
		int begin = 0;
		int end = 0;

		while (end < (int)s.size())
		{
			if (s[end] == ' ')
			{
				swapString(s, begin, end - 1);
				begin = end + 1;
				end = begin;           //将begin和end调到下个单词位置
			}
			else
			{
				end++;
			}
		}

		swapString(s, begin, end - 1); //最后一个单词的逆置

		swapString(s, 0, s.size() - 1);//再从头到尾逆置
	}

	void swapString(string &s, int begin, int end)
	{
		while (end > begin)
		{
			char c = s[begin];
			s[begin] = s[end];
			s[end] = c;
			begin++;
			end--;
		}
	}
};

int main()
{
	string str = "l like c++!";
	cout << "原单词顺序："<< str << endl;

	ReverseStr rstr;
	rstr.reverseWords(str);
	cout << "单词逆置后：" << str << endl;
	return 0;
}

