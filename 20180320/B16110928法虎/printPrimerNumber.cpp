#include <iostream>

using namespace std;

int printPrimeNumber(int num)
{
    int count=0;
    int i,j;
    for( i=2; i<=num; i++)
    {
        for( j=2; j<=i/2; j++)
        {
            if(i%j==0)
            break;
        }
        if(j>i/2)
        {
            count++;
            cout <<i <<" ";
            if(count%5!=0)continue;
            cout <<endl;
        }
    }
    return count;
}
int main()
{
    int n= printPrimeNumber(1000);

    cout<<"\n 1~100 Primer :" <<n <<endl;
    return 0;
}
