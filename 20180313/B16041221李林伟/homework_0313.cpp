#include<iostream>
using namespace std;

int main() {
	int a[] = {1,3,2,4,6,5,8,7,9}, n = 9;
	int max, min, sum;
	double average = 0;
	max = min = a[0];
	for(int i = 0; i < n; i++){
		max = a[i]>max?a[i]:max;  // 在加的时候判断出最大最小值 
		min = a[i]<min?a[i]:min;
		sum += a[i];
	}
	sum = sum - max - min; // 减去最大最小 
	average = (double)sum / (n-2);  // 求平均 
	cout << average << endl;
	return 0;
}
