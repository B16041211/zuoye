#include<stdio.h>

int main(void)
{
    int i;
    int a[9] = {1, 3, 2, 4, 6, 5, 8, 7, 9};
    int n = 9;
    int max_index = 0;
    int min_index = 0;
    double sum = 0;
    double average = 0;

    for (i = 0; i < n; i++)
    {
        if (a[max_index] < a[i])
            max_index = i;
        if (a[min_index] > a[i])
            min_index = i;
        sum += a[i];
    }
    sum -= (a[max_index] + a[min_index]);

    average = sum / (n - 2);
    printf("%lf\n", average);
    return 0;
}
