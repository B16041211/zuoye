#include <stdio.h>
#include <string.h>

int decode(char*s)
{
    int num=strlen(s);
//大小写字母转换
    for(int i=0; i<num; i++)
    {
        if(s[i]>='a' && s[i]<='z')
            s[i]+='A'-'a';
        else if(s[i]>='A' && s[i]<='Z')
            s[i]+='a'-'A';
    }
//转置
    int i=0,j=num-1;
    while(i<j)
    {
        char t=s[i];
        s[i]=s[j];
        s[j]=t;
        i++;
        j--;
    }
//右移三位
    for(int i=0; i<num; i++)
    {
        if(s[i]>='a' && s[i]<='z')
        {
            s[i]=(s[i]-'a'+3)%26+'a';
        }else if(s[i]>='A' && s[i]<='Z')
        {
            s[i]=(s[i]-'A'+3)%26+'A';
        }
    }

    return 0;
}

int main()
{
    char s[60]="";
    gets(s);
    decode(s);
    puts(s);
    return 0;
}
