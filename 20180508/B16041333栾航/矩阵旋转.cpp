#include<stdio.h>

void print(int a[4][4])
{
	int i;
	for(i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
		    printf("%3d",a[i][j]);
		    if(j>0&&j%3==0)
		    {
			    printf("\n");
		    }
	    }
	}
}

void left(int a[4][4],int b[4][4])
{
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
		    b[3-j][i]=a[i][j];	    
		}
	}
}

void right(int a[4][4],int b[4][4])
{
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			b[j][3-i]=a[i][j];    
		}
	}
}

int main()
{
	int i;
	int b[4][4];
	int a[4][4]={1,2,3,4,9,10,11,12,13,9,5,1,15,11,7,3};
	printf("原矩阵: \n");
	print(a);
	printf("\n");
	printf("选择变换方式：1.顺时针变换   2.逆时针变换 \n");
	scanf("%d",&i);
	if(i==1)
	{
		right(a,b);
		printf("顺时针旋转：\n");
		print(b);
	}
	if(i==2)
	{
		left(a,b);
		printf("逆时针旋转：\n");
		print(b);
	}
	return 0;
}
